package net.rgielen.spring.boot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
@Controller
@RequestMapping(path = "/hello")
public class HelloController {

    @RequestMapping(path = "/", method = RequestMethod.GET)
    public @ResponseBody String helloWorld() {
        return "Hello World";
    }

    @RequestMapping(path = "/", method = RequestMethod.GET, produces = "application/xml")
    public @ResponseBody String helloXmlWorld() {
        return "<xml>Hello World</xml>";
    }

}
