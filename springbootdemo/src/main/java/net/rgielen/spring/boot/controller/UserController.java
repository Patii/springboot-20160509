package net.rgielen.spring.boot.controller;

import net.rgielen.spring.boot.domain.User;
import net.rgielen.spring.boot.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
@RestController
@RequestMapping(path = "/users")
public class UserController {

    @Autowired
    UserRepository userRepository;

    @RequestMapping(path = "new", method = RequestMethod.GET)
    public User showNewUser() {
        return new User("foo", "bar", "foobar");
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public User findUserById(@PathVariable Long id) {
        return userRepository.findOne(id);
    }

    @RequestMapping(path = "/showalternative/{id}", method = RequestMethod.GET)
    public User findUserByIdWithSpringDataJpaWebSupprt(@PathVariable("id") User user) {
        return user;
    }
}
