package net.rgielen.spring.boot.repositories;

import net.rgielen.spring.boot.domain.Address;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
public interface AddressRepository extends JpaRepository<Address, Long>{

    List<Address> findAllByUserLastname(String lastname);
}
