package net.rgielen.spring.boot.repositories;

import net.rgielen.spring.boot.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.scheduling.annotation.Async;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
@Transactional
public interface UserRepository extends JpaRepository<User, Long> {

    User findFirstByUserName(String userName);

    Stream<User> findAllByLastname(String lastname);

    Page<User> findByLastname(String lastname, Pageable pageable);

//    Slice<User> findByLastname(String lastname, Pageable pageable);

    List<User> findByLastname(String lastname, Sort sort);

//    List<User> findByLastname(String lastname, Pageable pageable);

    Page<User> queryFirst10ByLastname(String lastname, Pageable pageable);

    User findFirstByOrderByLastnameAsc();

    @Async
    CompletableFuture<User> findOneByFirstname(String firstname);

    @Query("select u from User u where u.firstname = ?1 or u.lastname =?2")
    List<User> findByLastnameOrFirstname(String firstname, String lastname);

    @Query("select u from User u where u.lastname = :firstname or u.lastname = :lastname")
    List<User> findByLastnameOrFirstnameAlt(
            @Param("firstname") String firstname,
            @Param("lastname") String lastname);


    List<User> findByLastnameLike(String lastname);

    @EntityGraph("User.withAddresses")
    List<User> findAllByUserName(String username);

}
