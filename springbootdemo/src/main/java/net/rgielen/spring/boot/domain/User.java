package net.rgielen.spring.boot.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
@Entity
@NamedQueries({
        @NamedQuery(name = "User.findByLastnameLike",
                query = "select u from User u where u.lastname like ?1"
        ),
        @NamedQuery(name = "User.findByCity",
                query = "select u from Address a join a.user u where a.city=:city"
        ),
        @NamedQuery(name = "User.findUserWithItsAddresses",
                query = "select u from User u left outer " +
                        "join fetch u.addresses where u.userName = :username"
        )
})
@NamedEntityGraphs({
        @NamedEntityGraph(name = "User.withAddresses",
                attributeNodes = @NamedAttributeNode("addresses")
        )
})
@SequenceGenerator(name = "user_seq", sequenceName = "user_seq", allocationSize = 20)
public class User {

    @Id
    @GeneratedValue(generator = "user_seq")
    private Long id;
    private String firstname;
    private String lastname;
    private String userName;
    private String password;
    @Column(length = 1024)
    private String notes;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_ts")
    private Date created;
    @OneToMany(mappedBy = "user")
    private List<Address> addresses = new ArrayList<>();

    public User(String firstname, String lastname, String userName) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.userName = userName;
    }

    public User() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    @Transient
    public void addAddress(Address address) {
        address.setUser(this);
        getAddresses().add(address);
    }

    @Transient
    public String getFullname() {
        return getFirstname() + " " + getLastname();
    }
}
