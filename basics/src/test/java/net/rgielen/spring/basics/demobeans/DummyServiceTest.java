package net.rgielen.spring.basics.demobeans;

import net.rgielen.spring.basics.SpringBasicsConfig;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringBasicsConfig.class)
public class DummyServiceTest {

    @Autowired
    DummyService dummyService;

    @Test
    public void testServiceGetsInjected() throws Exception {
        assertNotNull(dummyService);
    }

    @Test
    public void testDaoGetsInjectedIntoService() throws Exception {
        assertNotNull(dummyService.dao);
    }

    @Test
    public void testDoSomethingWorks() throws Exception {
        final Object o = new Object();
        DummyDao stubDao = new DummyDao() {
            @Override
            public Object get(Long id) {
                return o;
            }
        };

        DummyService myDummyService = new DummyService(stubDao, new Foo());
        final int i = myDummyService.doSomethingWithDao(1l);

        assertEquals(o.hashCode(), i);
    }

    @Test
    public void testDoSomethingWorksUsingMocking() throws Exception {
        final Object o = new Object();
        final DummyDao mock = mock(DummyDao.class);
        System.out.println(mock.get(1l));
        when(mock.get(1l)).thenReturn(o);

        DummyService myDummyService = new DummyService(mock, new Foo());
        final int i = myDummyService.doSomethingWithDao(1l);
        assertEquals(o.hashCode(), i);

    }
}