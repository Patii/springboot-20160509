package net.rgielen.spring.basics;

import net.rgielen.spring.basics.annotations.TestEnvironment;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
@Configuration
public class SpringBasicsTestConfig {

    @Bean
    @TestEnvironment
    public DataSource dataSource() {
        return new EmbeddedDatabaseBuilder()
                .setName("testDb")
                .setType(EmbeddedDatabaseType.H2)
                .build();
    }

}
