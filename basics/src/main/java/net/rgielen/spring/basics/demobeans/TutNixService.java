package net.rgielen.spring.basics.demobeans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
@Component
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class TutNixService implements GenericService {

    @Autowired
    DummyDao dao;

}
