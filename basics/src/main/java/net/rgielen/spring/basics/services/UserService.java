package net.rgielen.spring.basics.services;

import net.rgielen.spring.basics.entities.User;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
@Component
@Transactional
public class UserService {

    @PersistenceContext
    EntityManager em;

    public void save(User user) {
        em.persist(user);
    }

}
