package net.rgielen.spring.basics.services;

import net.rgielen.spring.basics.entities.User;
import net.rgielen.spring.basics.repositories.TracingRepository;
import net.rgielen.spring.basics.repositories.UserRepositoryCustomMethods;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
@Component
public class UserRepositoryImpl implements UserRepositoryCustomMethods,TracingRepository {

    @PersistenceContext
    EntityManager em;

    public String createJsonRepresentation(User user) {
        return user.getFirstname();
    }

    public void logStartTime() {
        System.out.println("Hello");
    }
}
