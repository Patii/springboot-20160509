package net.rgielen.spring.basics;

import net.rgielen.spring.basics.annotations.Jndi;
import net.rgielen.spring.basics.demobeans.Bar;
import net.rgielen.spring.basics.demobeans.Foo;
import net.rgielen.spring.basics.demobeans.GenericService;
import net.rgielen.spring.basics.entities.User;
import net.rgielen.spring.basics.repositories.UserRepository;
import net.rgielen.spring.basics.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
@Configuration
@ComponentScan(basePackageClasses = {GenericService.class, UserService.class})
@PropertySources({
        @PropertySource("classpath:/spring-demo-config.properties"),
        @PropertySource(value = "file:./spring-demo-config.properties", ignoreResourceNotFound = true),
        @PropertySource(value = "file:${user.home}/spring-demo-config.properties", ignoreResourceNotFound = true)
})
@EnableTransactionManagement(proxyTargetClass = true)
@EnableJpaRepositories(basePackageClasses = UserRepository.class)
public class SpringBasicsConfig {

    @Autowired
    Environment env;

    @Bean
    @Profile("default")
    public DataSource dataSource() {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(org.h2.Driver.class.getName());
        dataSource.setUrl(env.getProperty("db.url"));
        dataSource.setUsername(env.getProperty("db.user"));
        dataSource.setPassword(env.getProperty("db.password"));
        return dataSource;
    }

    @Jndi @Bean
    public DataSource productionDataSource() throws Exception {
        Context ctx = new InitialContext();
        return (DataSource) ctx.lookup("java:comp/env/jdbc/datasource");
    }

    @Bean @Autowired
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource) {
        LocalContainerEntityManagerFactoryBean emfb = new LocalContainerEntityManagerFactoryBean();
        emfb.setDataSource(dataSource);
        emfb.setPackagesToScan(User.class.getPackage().getName());

        emfb.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        emfb.setJpaProperties(myHibernateProperties());
        return emfb;
    }

    @Bean @Autowired
    public PlatformTransactionManager transactionManager(EntityManagerFactory emf){
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(emf);
        return transactionManager;
    }

    private Properties myHibernateProperties() {
        Properties properties = new Properties();
        properties.setProperty("hibernate.hbm2ddl.auto", "update");
        properties.setProperty("hibernate.id.new_generator_mappings", "true");
        return properties;
    }

    @Profile("!default")
    public Object someObject() {
        return new Object();
    }

    @Bean
    public Bar myBar() {
        return new Bar();
    }

    @Bean @Autowired
    public Foo myFoo(Bar bar) {
        final Foo foo = new Foo();
        foo.setBar(bar);
        return foo;
    }

}
