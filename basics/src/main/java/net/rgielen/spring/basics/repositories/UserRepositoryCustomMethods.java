package net.rgielen.spring.basics.repositories;

import net.rgielen.spring.basics.entities.User;

/**
 * @author <a href="mailto:rene.gielen@gmail.com">Rene Gielen</a>
 */
public interface UserRepositoryCustomMethods {

    String createJsonRepresentation(User user);

}
